import Input from './Input';

class InputsContainer {
	constructor(classNameContainer = '') {
		this.container = document.createElement('div');

		this.container.className = classNameContainer || '';

		this.renderInputs();
	}

	renderInputs() {
		const inputsList = [
			{
				name: 'name',
				value: '',
				className: '',
			},
		];

		const objectsToRender = inputsList.map((input) =>
			new Input(input.name, input.value, input.className).getElement()
		);

		objectsToRender.forEach((element) => this.container.append(element));
	}

	getElement() {
		return this.container;
	}
}

export default InputsContainer;
