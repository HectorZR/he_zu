class TextBox {
	constructor(value = '', className = '', keyName = '', icon = '') {
		this.value = value;
		this.className = className;
		this.icon = icon;
		this.keyName = keyName;
		this.imgIcon;

		this.container = document.createElement('div');
		this.pElement = document.createElement('p');

		this.setImageIcon();
		this.setTexBoxConfigs();
	}

	setTexBoxConfigs() {
		this.pElement.textContent = this.value;
		this.container.className = 'text-box';
		this.container.className =
			this.container.className + ' ' + this.className;

		this.container.append(this.pElement);
	}

	setImageIcon() {
		if (this.icon) {
			this.imgIcon = document.getElementById(this.icon);
			this.imgIcon.style.display = '';
			this.container.append(this.imgIcon);
		}
	}

	getElement() {
		return this.container;
	}
}

export default TextBox;
