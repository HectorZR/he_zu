
class ProfilePic {
	constructor(profileImage = '', className = '') {
		this.container = document.createElement('div');
		this.profilePic = document.createElement('img');

		this.profilePic.src = profileImage;

		this.container.className = className;
		this.container.append(this.profilePic);
	}

	getElement() {
		return this.container;
	}
}

export default ProfilePic;
