class Button {
	constructor(innerText = '', onclick = () => {}, className = '') {
		this.button = document.createElement('button');
		this.container = document.createElement('div');

		this.container.className = className;
		this.button.innerText = innerText;

		this.button.onclick = onclick;

		this.container.append(this.button);
	}

	getElement() {
		return this.container;
	}
}

export default Button;
