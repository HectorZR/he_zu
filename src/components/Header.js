import ProfilePic from './ProfilePic';
import Button from './Button';
import IconButton from './IconButton';
import Navbar from './Navbar';
import ProfileImage from '../assets/img/profile_image.jpg';
import TextBoxContainer from './TextBoxContainer';

class Header {
	constructor() {
		this.header = document.createElement('header');

		this.header.classList.add('header');

		this.elementsToRender = [
			new Button(
				'Log out',
				function () {
					console.log('clicked me!');
				},
				'header__button-logout'
			),
			new IconButton(
				'camera-icon',
				'Upload cover image',
				function () {
					console.log('picture uploaded');
				},
				'header__button-upload'
			),
			new ProfilePic(ProfileImage, 'header__pic'),
			new TextBoxContainer(),
			new Navbar(
				['about', 'settings', 'option1', 'option2', 'option3'],
				'header__navbar'
			),
		];

		this.elementsToRender.forEach((element) =>
			this.header.append(element.getElement())
		);
	}

	getElement() {
		return this.header;
	}
}

export default Header;
