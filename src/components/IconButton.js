class IconButton {
	constructor(icon = '', text = '', onclick = () => {}, className = '') {
		this.iconButton = document.createElement('button');
		this.container = document.createElement('div');
		this.svgIcon = document.getElementById(icon);
		this.titleText = document.createElement('p');

		this.svgIcon.src = icon;
		this.svgIcon.style.display = '';
		this.titleText.innerText = text;

		this.iconButton.onclick = onclick;
		this.iconButton.append(this.svgIcon);
		this.iconButton.append(this.titleText);

		this.container.className = className;
		this.container.append(this.iconButton);
	}

	getElement() {
		return this.container;
	}

	getElementToString() {
		const temporalNode = document.createElement('div');
		temporalNode.append(this.container);

		return temporalNode.innerHTML;
	}
}

export default IconButton;
