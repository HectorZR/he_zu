class Input {
	constructor(name = '', value = '', className = '') {
		this.name = name;
		this.value = value;
		this.className = className;

		this.input = document.createElement('input');

		this.label = document.createElement('label');

		this.container = document.createElement('div');

		this.setInputConfigs();
	}

	setInputConfigs() {
		this.input.name = this.name;
		this.input.value = this.value;
		this.input.id = this.name;

		this.label.innerHTML = this.name;
		this.label.htmlFor = this.name;

		this.container.className =
			this.container.className + ' ' + this.className;
		this.container.append(this.label);
		this.container.append(this.input);
	}

	getElement() {
		return this.container;
	}
}

export default Input;
