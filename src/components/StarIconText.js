class StarIconText {
	constructor(fullStars = 0, emptyStars = 0, text = '') {
		this.emptyStars = emptyStars;
		this.fullStars = fullStars;
		this.text = text;
		this.container = document.createElement('div');
		this.icon;
		this.pElement = document.createElement('p');

		this.container.className = 'header__star-icon-container';
		this.setStars('full-star-icon', this.fullStars);
		this.setStars('empty-star-icon', this.emptyStars);
		this.setText();
	}

	setStars(icon, times) {
		for (let index = 0; index < times; index++) {
			this.icon = document.getElementsByClassName(icon)[0];

			this.icon.style.display = '';

			this.container.append(this.icon);
		}
	}

	setText() {
		this.pElement.textContent = this.text;
		this.container.append(this.pElement);
	}

	getElement() {
		return this.container;
	}
}

export default StarIconText;
