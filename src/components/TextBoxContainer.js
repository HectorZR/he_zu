import { getData } from '../utils/objects';
import StarIconText from './StarIconText';
import TextBox from './TextBox';

class TextBoxContainer {
	constructor() {
		this.container = document.createElement('div');
		this.textBoxesContainer = document.createElement('div');

		this.container.className = 'header__text-box-main';
		this.textBoxesContainer.className = 'header__text-box-container';

		this.setTextBoxes();
		this.setStars();
	}

	setTextBoxes() {
		const globalState = getData(window, '__globalState');
		const textsToRender = [
			{
				text: getData(globalState, 'name'),
				className: 'text-box--title',
				icon: '',
			},
			{
				text: getData(globalState, 'address'),
				className: '',
				icon: 'location-icon',
			},
			{
				text: getData(globalState, 'cellphone'),
				className: '',
				icon: 'phone-icon',
			},
		];

		textsToRender.forEach((textToRender) => {
			const tempTextBox = new TextBox(
				textToRender.text,
				textToRender.className,
				textToRender.keyName,
				textToRender.icon
			);

			this.textBoxesContainer.append(tempTextBox.getElement());
		});

		this.container.append(this.textBoxesContainer);
	}

	setStars() {
		this.container.append(new StarIconText(1, 1, '6 reviews').getElement());
	}

	getElement() {
		return this.container;
	}
}

export default TextBoxContainer;
