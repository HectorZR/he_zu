import { getData } from '../utils/objects';
import IconButton from './IconButton';

class Navbar {
	constructor(navbarOptions = [], classNameNavbar = '') {
		this.navbarOptions = navbarOptions;
		this.navbar = document.createElement('nav');
		this.navbar.className = classNameNavbar;

		this.navbar.innerHTML = this.renderNavbarContainer();
    this.addFollower = this.addFollower.bind(this);
    this.getMenuOption = this.getMenuOption.bind(this)
	}

	renderNavbarOptions() {
		return this.navbarOptions.map(
			(option) => (`
        <div id="${option}" onclick="__getMenuOption(event)">${option}</div>
      `) 
		);
	}

	getMenuOption(event) {
    const globalState = getData(window, '__globalState')

    globalState.menuOption = event.target.id
	}

	renderNavbarContainer() {
    window.__getMenuOption = this.getMenuOption
    
		return `
      <div class="header__navbar-menu" >
        ${this.renderNavbarOptions().join(' ')}
      </div>
      <div class="header__navbar-followers">${this.renderFollowers()}</div>
    `;
	}

	renderFollowers() {
		return new IconButton(
			'add-icon',
			`${this.getFollowersNumber()} followers`,
			this.addFollower,
			'header__button-followers',
		).getElementToString();
	}

	getFollowersNumber() {
    return getData(window, '__globalState.followers');
	}

	addFollower() {
		const followersNumber = parseInt(
			getData(window, '__globalState.followers'),
			10
		);
		if (typeof followersNumber === 'number') {
			localStorage.setItem('followers', followersNumber + 1);
			return;
		}
		localStorage.setItem('followers', 0);
	}

	getElement() {
		return this.navbar;
	}
}

export default Navbar;
