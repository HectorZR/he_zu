/**
 * Set in localStore default values of app
 */
export function setLocalStorageDefaults() {
	const defaultValues = {
		followers: 15,
		name: 'jessica parker',
		cellphone: '(949) 325 - 68594',
		address: 'Newport Beach, CA',
	};

	Object.keys(defaultValues).forEach((key) => {
		if (localStorage.getItem(key)) {
			return;
		}
		localStorage.setItem(key, defaultValues[key]);
	});
}

/**
 * Set in window object a global state to get in app from everywhere
 */
export function getLocalStorageDefaults() {
	const defaultKeys = ['followers', 'name', 'cellphone', 'address'];

	const temporalState = {};

	defaultKeys.forEach((key) => {
		temporalState[key] = localStorage.getItem(key);
	});

	temporalState.menuOption = 'about';
	window.__globalState = temporalState;
}
