import Header from './components/Header';
import InputsContainer from './components/InputsContainer';
import { getLocalStorageDefaults, setLocalStorageDefaults } from './configs';
import './sass/index.sass';

setLocalStorageDefaults();
getLocalStorageDefaults();

const rootContainer = document.getElementsByTagName('body')[0];

rootContainer.insertBefore(
	new InputsContainer('input-container').getElement(),
	rootContainer.firstChild
);
rootContainer.insertBefore(new Header().getElement(), rootContainer.firstChild);

