/**
 * Access a given object to safely get the data
 * @param {{}} object access object to get data
 * @param {String} keys path to the needed value
 * @return {String} data
 */
export function getData(object = {}, keys = '') {
	let path = keys.split('.');

	let data = object;

	if (path) {
		path.forEach((key) => {
			if (typeof data === 'object') {
				if (typeof data[key] !== 'undefined' && data[key]) {
					data = data[key];
					return;
				}
				data = keys;
			}
		});
	}
	return data;
}
