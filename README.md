# HE_ZU
A single page app done with Webpack and SASS, following BEM for CSS

### Clone it
```git
git clone https://HectorZR@bitbucket.org/HectorZR/he_zu.git 
```

### Install dependencies
```sh
cd he_zu && npm install
```

### Run it
This will work to develope more in the project, the is served on http:localhost:8080/
```sh
npm run dev
```

### Build it
This will generate a folder called build inside the project
```sh
npm run build
```